
/**
 * This class stores cluster information for candidate atlas mapped points.
 * Candidate point clusters may have different weights, based on their location.
 * Since highlighting a cluster point, which is close to center of the brain, is more critical
 * and gives better and accurate results, those clusters have more weight than the others. 
 * 
 * */

public class CSCandidatePointCluster
{
	/** Total point count in the cluster. */
	private int pointCount = 0;
	
	/** Total weight of the cluster. */
	private int totalWeight = 0;
	
	/** True if the candidate point cluster belongs to left CS. */
	private boolean leftCS = true;
	
	
	public int getPointCount()
	{
		return pointCount;
	}
	
	public void setPointCount(int pointCount)
	{
		this.pointCount = pointCount;
	}

	public int getTotalWeight()
	{
		return totalWeight;
	}

	public void setTotalWeight(int totalWeight)
	{
		this.totalWeight = totalWeight;
	}

	public boolean isLeftCS()
	{
		return leftCS;
	}

	public void setLeftCS(boolean leftCS)
	{
		this.leftCS = leftCS;
	}
	
	
	
}
