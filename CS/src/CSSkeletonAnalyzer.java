import ij.process.ImageProcessor;

import java.awt.Point;
import java.util.ArrayList;


public class CSSkeletonAnalyzer
{
	private boolean[][] binaryData;
	
	private int iterationCount = 0;
	private boolean firstPoint = false;
	
	
	public boolean[][]  getCentralSulcusBranch(boolean[][] binaryData, Point point)
	{

		this.binaryData = binaryData;
		//segmentation = new boolean[binaryData.length][binaryData[0].length];
		
		int radius = 20; // TODO value should be in mm.

		Point conflictPoint = new Point(0,0);
	
		for(int r=0; r < radius; r++)
		{
			if( binaryData[point.x][point.y - r])
			{
				conflictPoint = new Point(point.x, point.y - r);
				break;
			}
			else if( binaryData[point.x][point.y + r])
			{
				conflictPoint = new Point(point.x, point.y + r);
				break;
			}
			else if( binaryData[point.x - r][point.y])
			{
				conflictPoint = new Point(point.x - r, point.y);
				break;
			}
			else if( binaryData[point.x + r][point.y])
			{
				conflictPoint = new Point(point.x + r, point.y);
				break;
			}
		}
		
		if (conflictPoint.x + conflictPoint.y > 0) // There is branch conflict
		{
			firstPoint = true;
			ArrayList<Point> neighbors = new ArrayList<Point>();
			neighbors.add(conflictPoint);
			
			//System.out.println("Starting to analyze skeletong with point ("+ conflictPoint +")");
			
			boolean[][] _data = getCentralSulcusBranchEx(neighbors, true, null, true);
			return _data;
		}
	
		iterationCount = 0;
		
		return null;
	}
	
	
	
	
	private boolean[][] getCentralSulcusBranchEx( ArrayList<Point> points, boolean mainBranch, boolean[][] currentSegmentation, boolean firstPoint)
	{
		int pointCountThreshold = 12;
		int pointCount = 0;
		
		
		boolean[][] _tempSegmentation = new boolean[binaryData.length][binaryData[0].length];
	    boolean addedBranch = false;
	    
	    // get finalized segmentation
	    if(currentSegmentation != null)
	    	mergeArrayOver(currentSegmentation, _tempSegmentation);

	    
	 	for(Point point:points)
	 	{
	 		Point currentPoint = point;
	 		boolean continueToLoop = true;

	 		while(continueToLoop)
	 		{
	 			_tempSegmentation[currentPoint.x][currentPoint.y] = true;
	 			
	 			ArrayList<Point> neighbors = getUnvisitedNeighbors(currentPoint, _tempSegmentation);
	 			
	 			if (neighbors.size() == 0) // end of branch
	 			{
	 				//System.out.println("End of branch at point ("+ currentPoint +")");
	 				pointCount++;
	 				if(mainBranch || pointCount < pointCountThreshold)
	 				{
	 					addedBranch = true;
	 				}
	 				continueToLoop = false;
	 			}
	 			else if (neighbors.size() == 1 ) // normal point on skeleton
	 			{
	 				pointCount++;
	 				currentPoint = neighbors.get(0);
					_tempSegmentation[currentPoint.x][currentPoint.y] = true;
	 			}
	 			else if(neighbors.size() >=2 ) // joint
	 			{
	 				//System.out.println("New joint at point ("+ currentPoint +")");
	 				//System.out.println("        Main branch("+mainBranch+"), pointCount("+pointCount+"), pointCountThreshold("+pointCountThreshold+"), firstPoint("+firstPoint+")");
	 				if((mainBranch && pointCount < (pointCountThreshold * 5)) || (pointCount < pointCountThreshold))
	 				{
	 					addedBranch = true;
	 					
	 					boolean isMainBranch =  (firstPoint || (mainBranch && pointCount < pointCountThreshold));
	 					boolean[][] _returnedData = getCentralSulcusBranchEx(neighbors, isMainBranch, _tempSegmentation, false);
		 				
		 				if(_returnedData != null)
		 				{
		 					mergeArrayOver(_returnedData, _tempSegmentation);
		 				}
	 				}
	 				
	 				continueToLoop = false;
	 
	 			}
	 		}

	 	}
	 	
	 	
	 	if(addedBranch)
	 	{
	 		return _tempSegmentation;
	 	}
	 	
	 	return null;
	}
	
	
	
	
	private ArrayList<Point> getUnvisitedNeighbors(Point point, boolean[][] data)
	{
		ArrayList<Point> returnValue = new ArrayList<Point>();
		int xOrder[] = {0,1,1,1,0,-1,-1,-1};
		int yOrder[] = {1,1,0,-1,-1,-1,0,1};
		
		// Find unvisited neighbors
		for(int i=0; i < 8; i++)
		{
			int normalizedX = point.x + xOrder[i]; 
			int normalizedY = point.y + yOrder[i]; 
			
			boolean isVisited = data[normalizedX][normalizedY];
			
			if(!isVisited && binaryData[normalizedX][normalizedY]) // Active point for skeleton
			{
				returnValue.add(new Point(normalizedX, normalizedY));
			}
		}
		
		return returnValue;
	}
	
//	private void getCentralSulcusBranch( Point startPoint, boolean mainBranch )
//	{
//		iterationCount++;
//		
//		ArrayList<Point> pointQueue = new ArrayList<Point>();
//		int currentIndex = 0;
//		int pointCountThreshold = 15;
//		int pointCount = 0;
//		
//		int xOrder[] = {0,1,1,1,0,-1,-1,-1};
//		int yOrder[] = {1,1,0,-1,-1,-1,0,1};
//		
//		boolean[][] _tempSegmentation = new boolean[binaryData.length][binaryData[0].length];
//	    boolean addedBranch = false;
//		
//		// get finalized segmentation
//		mergeArrayOver(segmentation, _tempSegmentation);
//		
//		pointQueue.add(startPoint);
//		//segmentation[startPoint.x][startPoint.y] = true;
//		
//		_tempSegmentation[startPoint.x][startPoint.y] = true;
//		pointCount++;
//		
//		ArrayList<Point> neighbors = new ArrayList<Point>();
//		while(currentIndex < pointQueue.size() && iterationCount < 10)
//		{
//			Point currentPoint = pointQueue.get(currentIndex);
//			neighbors.clear();
//			
//			// Find unvisited neighbors
//			for(int i=0; i < 8; i++)
//			{
//				int normalizedX = currentPoint.x + xOrder[i]; 
//				int normalizedY = currentPoint.y + yOrder[i]; 
//				
//				boolean imageValue = binaryData[normalizedX][normalizedY];
//				boolean isVisited = _tempSegmentation[normalizedX][normalizedY];;
//				
//				if(imageValue && !isVisited) // Active point for skeleton
//				{
//					neighbors.add(new Point(normalizedX, normalizedY));
//				}
//			}
//			
//			
//			if(neighbors.size() >= 2 && !firstPoint) // Joint point to other branches
//			{
//				if(mainBranch || (!mainBranch && pointCount < pointCountThreshold) || addedBranch)
//				{
//					System.out.println("Added branch with point count(" + pointCount + ")");
//					mergeArrayOver(_tempSegmentation, segmentation);
//					addedBranch = true;
//				}
//				
//				System.out.println("found new branch");
//				
//				for(Point neighbor:neighbors)
//				{
//					getCentralSulcusBranch(neighbor, false);
//				}
//			}
//			else if(neighbors.size() == 1 || firstPoint) // Normal point on skeleton
//			{
//				for(Point neighbor:neighbors)
//				{
//					pointQueue.add(neighbor);
//					_tempSegmentation[neighbor.x][neighbor.y] = true;
//					pointCount++;
//				}
//			}
//			else if(neighbors.size() == 0) // End of branch
//			{
//				if(mainBranch || (!mainBranch && pointCount < pointCountThreshold) || addedBranch)
//				{
//					System.out.println("Added branch with point count(" + pointCount + ")");
//					mergeArrayOver(_tempSegmentation, segmentation);
//					addedBranch = true;
//				}
//			}
//
//			firstPoint = false;
//			currentIndex++;
//		}
//		
//		if(mainBranch || (!mainBranch && pointCount < pointCountThreshold))
//		{
//			mergeArrayOver(_tempSegmentation, segmentation);
//		}
//
//		if(!addedBranch)
//		{
//			System.out.println("Not added branch, sorry :-(");
//			iterationCount = 10;
//		}
//	}
	
	private void mergeArrayOver(boolean[][] arrayToBeMerged, boolean[][] targetArray)
	{
		int width = arrayToBeMerged.length;
		int height = arrayToBeMerged[0].length;
		for(int x = 0; x < width; x++)
		{
			for(int y = 0; y < height; y++)
			{
				if(arrayToBeMerged[x][y])
				{
					targetArray[x][y] = true;
				}
					
			}
		}
	}
	
}
