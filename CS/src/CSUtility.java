import java.awt.Color;

import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;


public class CSUtility
{

	public static void showImageProcessor(String name, ImageProcessor processor)
	{
		ImageStack _stack = new ImageStack(processor.getWidth(), processor.getHeight());
		_stack.addSlice(processor);
		
		showStack(name, _stack);
	
	}


	public static void showStack(String name, ImageStack stack)
	{
		ImagePlus plus = new ImagePlus(name, stack);
		plus.show();		
	}
	
	public static void showBinaryData(String name, boolean[][] binaryData)
	{
		ImageProcessor processor = generateSliceWithBinaryData(binaryData);
		showImageProcessor(name, processor);		
	}
	
	public static ImageProcessor generateSliceWithBinaryData(boolean[][] binaryData)
	{
		int width = binaryData.length, height = binaryData[0].length;
		ImageProcessor processor = new  ByteProcessor(width, height);
		
		processor.setColor(Color.WHITE);

		for (int activeMatrixX = 0; activeMatrixX < processor.getWidth(); activeMatrixX++)
		{
			for (int activeMatrixY = 0; activeMatrixY < processor.getHeight(); activeMatrixY++)
			{
				if (binaryData[activeMatrixX][activeMatrixY] == true)
				{
					processor.drawPixel(activeMatrixX, activeMatrixY);
				}
			}
		}

		return processor;
	}
	
	public static boolean[][] generateBinaryData(ImageProcessor processor)
	{
		boolean[][] _binaryData = new boolean[processor.getWidth()][processor.getHeight()];

		
		for (int x=0; x < processor.getWidth(); x++)
		{
			for (int y=0; y < processor.getHeight(); y++)
			{
				_binaryData[x][y] = (processor.getPixel(x, y) > 0);
			}
		}

		return _binaryData;
	}
	
	public static boolean[][] mergeBinaryData(boolean[][] source, boolean[][] target)
	{
		if(source == null) { System.out.println("Left CS couldn't be found"); }
		if(target == null) { System.out.println("Right CS couldn't be found"); }
		
		int width = source.length, height = source[0].length;
		boolean[][] _binaryData = new boolean[width][height];

		
		for (int x=0; x < width; x++)
		{
			for (int y=0; y < height; y++)
			{
				if(source != null)
				{
					_binaryData[x][y] = source[x][y];
					
					if(target!= null &&  target[x][y])
					{
						_binaryData[x][y] = target[x][y];
					}
				}
				else if(source == null && target != null)
				{
					_binaryData[x][y] = target[x][y];
				}
				else
				{
					_binaryData[x][y] = false;
				}
			}
		}

		return _binaryData;
		
	}
}
