import java.io.File;
import java.io.FilenameFilter;

import javax.swing.filechooser.FileNameExtensionFilter;

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.PlugIn;


public class CS_GetDiceScores implements PlugIn
{
	@Override
	public void run(String arg)
	{
		// TODO Auto-generated method stub
		
		String sourcePath = "./../../Data/BAYINDIR_DATA/Original/";
		
		File sourceFolder = new File(sourcePath);
		
		String files[] = sourceFolder.list(new FilenameFilter()
		{
			
			@Override
			public boolean accept(File arg0, String arg1)
			{
				// TODO Auto-generated method stub
				return arg1.endsWith("hdr");
			}
		});
		
		for(String fileName:files)
		{
			String filePath = sourcePath + fileName;
			
			ImagePlus ij = new ImagePlus(filePath);
			ij.show();
			
			Segment_CS csSegmentor = new Segment_CS();
			csSegmentor.setup("", ij);
			csSegmentor.run(ij.getProcessor());
			
			ij.updateAndDraw();
		}
		
	}
}
