

//
// Multi_OtsuThreshold_.java
//
// Algorithm: PS.Liao, TS.Chen, and PC. Chung,
//            Journal of Information Science and Engineering, vol 17, 713-727 (2001)
// 
// Coding   : Yasunari Tosa (ytosa@att.net)
// Date     : Feb. 19th, 2005
//
import ij.ImagePlus;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Random;
import java.util.Vector;

public class MidsaggitalUtil  {

    
    public ImagePlus imp;
    static final int NGRAY = 256;
    private double minimumDistance = Double.MAX_VALUE;
    private Line2D bestLine = null; 
    
   

    public MidsaggitalInformation getMidsaggitalInformation(ImageProcessor ip) {

        minimumDistance = Double.MAX_VALUE;
        bestLine = null; 
        
        Vector<Point> points = new Vector<Point>();
        MidsaggitalInformation returnValue = new MidsaggitalInformation();
        
        boolean drawInfo = false;
        
        double meanValue = ip.getStatistics().mean;
        int yUnit = ip.getHeight() / 4;
        int yCenterOfMass = (int)ip.getStatistics().yCenterOfMass;
        

        ip.setColor(Color.WHITE);
         
        
        for (int r = yCenterOfMass - yUnit; r < yCenterOfMass + yUnit; r++) 
        {
        	int xValues = 0;
        	int pointCount = 0;
        	
            for (int c = (ip.getWidth() / 5) * 2 ; c < ((ip.getWidth() / 5) * 3) ; c++) 
            {
                int pixelValue = ip.getPixel(c, r);

                if (pixelValue  < meanValue ) 
                {
                	xValues += c;
                	pointCount++;
                }
            }

            if(pointCount > 0)
            {
            	Point point =  new Point( (int) ((float) xValues / (float) pointCount), r );
                points.add(point);
                if(drawInfo) ip.drawPixel(point.x, point.y);
            }
            
        }
        
//        double xMean = 0.0;
//        for(int i=0; i < points.size(); i++)
//        {
//            Point point = points.elementAt(i);
//            xMean += point.x;
//        }
//        
//        xMean = xMean / points.size();
//        
//        Vector<Point> newPoints = new Vector<Point>();
//        for(int i=0; i < points.size(); i++)
//        {
//            Point point = points.elementAt(i);
//            
//            int x = point.x;
//            
//            if(Math.abs(x - xMean) < 20)
//            {
//                newPoints.add(point);
//                ip.drawPixel(point.x, point.y);
//
//            }
//        }
//        
//        points = newPoints;
        
        for(int i=0; i < points.size(); i++)
        {
            bestLine(points, ip);
        }
        
        double slope = (bestLine.getX2() - bestLine.getX1()) / (bestLine.getY2() - bestLine.getY1());
        
        double x1 = bestLine.getX1() - (bestLine.getY1() * slope);
        double y1 = 0;
        double x2 = bestLine.getX2() + ((ip.getHeight() - bestLine.getY2()) * slope);
        double y2 = ip.getHeight();
        
        if(drawInfo)
        {
            ip.drawLine((int) x1, (int)y1, (int)x2, (int)y2);
        }
        
        returnValue.pointOne = new Point((int)x1, (int)y1);
        returnValue.pointTwo = new Point((int)x2, (int)y2);
        
       // IJ.showMessage("avg value : " + ((float)minimumDistance / (float)points.size()));
        // Correct rotation
        
        Point point1 = new Point((int)x1, (int)y1);
        Point point2 = new Point((int)x2, (int)y2);
        //point1.
        
        double angle = getAngle(point1, point2);
        
        //ip.setInterpolationMethod(ImageProcessor.BILINEAR);
        //ip.rotate(angle);
        
        //loadAtlasImage(ip);
        
        returnValue.candidatePointsCount = points.size();
        returnValue.rotationAngle = angle;
        returnValue.average = ((float)minimumDistance / (float)points.size());
        
        return returnValue;
    }
    
    public double getAngle(Point p1, Point p2)
    {
        double xDiff = p2.x - p1.x;
        double yDiff = p2.y - p1.y;
        return 90d - Math.toDegrees(Math.atan2(yDiff, xDiff));
    }
    
    
    public void bestLine(Vector<Point> points, ImageProcessor ip)
    {
        double totalDistance = 0.0d;
        // Choose random two points
        Random randomGenerator = new Random();
        int randomInt1 = randomGenerator.nextInt(points.size());
        int randomInt2 = randomGenerator.nextInt(points.size());
        
        Point2D point1 = (Point2D)points.elementAt(randomInt1);
        Point2D point2 = (Point2D) points.elementAt(randomInt2);
   
        Line2D.Double line = new Line2D.Double(point1, point2);
        
        for(int i=0; i < points.size(); i++)
        {
            Point2D point = points.elementAt(i);
            
            if(i==randomInt1 || i==randomInt2)
            {
                continue;
            }
            
            double distance = line.ptLineDist(point);
            totalDistance += distance;
        }
        
        if(totalDistance < minimumDistance)
        {
            minimumDistance = totalDistance;
            bestLine = line;
        }
        
    }
}
