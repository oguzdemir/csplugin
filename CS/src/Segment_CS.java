import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.ImageRoi;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.plugin.filter.Analyzer;
import ij.plugin.filter.Binary;
import ij.plugin.filter.MaximumFinder;
import ij.plugin.filter.PlugInFilter;
import ij.process.ByteProcessor;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import ij.process.ShortProcessor;
import ij.text.TextWindow;
import ij.util.DicomTools;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.UIDefaults.ActiveValue;

public class Segment_CS implements PlugInFilter
{

	/** ImagePlus reference */
	private ImagePlus imp = null;
	
	/** Stack reference */
	private ImageStack stack = null; 
	
	/** Stores slice thickness */
	private double voxelDepth = 0.0d; 
	
	/** It stores positive pixel counts for  each stack */
	private int stackSlicePixelCount[];  
	
	/** It stores maximumPixelCount normalized values of each slice (stackSlicePixelCount[x] / maximumPixelCount ) */
	private double normalizedValues[];
	
	/**  */
	private int maximumPixelCount = 0;
	
	/** It stores slice index having maximum  pixel count */
	private int maximumSliceIndex = 0;
	
	/** It direction of the slices in the stack. (-1) means slices ordered from top of the skull to bottom. (1) is vice versa. */
	private int slicesDirection;
	
	/** Stores current slice index. */
	private int currentSliceIndex = 0;

	/** It stores segmented points in 3d matrix (boolean[sliceIndex][x][y]) */
	private boolean[][][] activeMatrix;
	
	/** It stores skeleton branch points of CS */
	private boolean[][] activeCSBranch;
	
	/** It stores pixel visit information of CS branch */
	private boolean[][] activeCSBranchVisitMatrix;
	
	/** It stores pixel count of segmented CS region. */
	private int csBranchSegmentedPixelCount = 1;
	
	/** True if new points were added to segmentation region during regionGrow() */
	private boolean newPointsAdded = false; //
	
	/** Skull rectangle for active slice */
	private Rectangle skullRectangle = null;

	/** Left CS candidate segmentation start point */
	private Point leftCSSegmentationCandidate = null;
	
	/** Original atlas image. */
	private BufferedImage originalAtlasImage = null;
	
	/** Mapped atlas image. This image has the same size with current image processor.
	 *  It contains original atlas image which is mapped to current slice coordinate space */
	private BufferedImage mappedAtlasImage = null;
	
	@Override
	public int setup(String arg, ImagePlus imp)
	{
		this.imp = imp;
		return DOES_ALL;
	}

	@Override
	public void run(ImageProcessor ip)
	{

		IJ.run(imp, "Despeckle", "stack");
		
		stack = imp.getStack();
		voxelDepth = imp.getFileInfo().pixelDepth;

		this.findKeySlice(false); // false:find slice, true: use selected slice
		
		ImageProcessor keySliceImageProcessor = stack.getProcessor(currentSliceIndex);
		activeMatrix = new boolean[stack.getSize()][keySliceImageProcessor.getWidth()][keySliceImageProcessor.getHeight()];
		
		boolean[][] leftCS = this.segmentLeftCSSulcus(keySliceImageProcessor);
		boolean[][] rightCS = this.segmentRightCSSulcus(keySliceImageProcessor);
		
		boolean[][] cs = CSUtility.mergeBinaryData(leftCS, rightCS);
		
		ImageProcessor csProcessor = CSUtility.generateSliceWithBinaryData(cs);
		CSUtility.showImageProcessor("CS", csProcessor);
		
		System.out.println("Dice Score for ("+ imp.getTitle() +") : " + getDiceScore(cs));
	}

	
	private boolean[][] segmentRightCSSulcus(ImageProcessor keySliceImageProcessor)
	{
		// Find bast matching atlas image. originalAtlasImage and mappedAtlasImage variables were set in this function.
		this.findBestRightCSAtlas(keySliceImageProcessor);

		// Grow region
		this.growRegion(keySliceImageProcessor, false);
		//CSUtility.showBinaryData(imp.getTitle() +  " Initial Segmentation", activeMatrix[currentSliceIndex - 1]);
		
		// Skeletonize image
		ImageProcessor skeletonImageProcessor = new ByteProcessor(keySliceImageProcessor.getWidth(), keySliceImageProcessor.getHeight());		
		skeletonImageProcessor = CSUtility.generateSliceWithBinaryData(activeMatrix[currentSliceIndex - 1]).convertToByteProcessor();
		skeletonImageProcessor.xor(Integer.parseInt("11111111", 2));
		
		Binary binary = new Binary();
		binary.setup("fill", new ImagePlus("Skeleton Image Processor", skeletonImageProcessor));
		binary.run(skeletonImageProcessor);

		skeletonImageProcessor.xor(Integer.parseInt("11111111", 2));

		Skeletonize3D_ skeletonize3d = new Skeletonize3D_();
		skeletonize3d.setup("", new ImagePlus("Skeleton Image Processor", skeletonImageProcessor));
		skeletonize3d.run(skeletonImageProcessor);

		boolean[][] skeletonBinaryData = CSUtility.generateBinaryData(skeletonImageProcessor);

		//CSUtility.showBinaryData(imp.getTitle() +  " SKELETON", skeletonBinaryData);
		
		// Analyze skeletonized version of segmentation and get Central Sulcus branch.
		CSSkeletonAnalyzer skeletonAnalyzer = new CSSkeletonAnalyzer();
		activeCSBranch = skeletonAnalyzer.getCentralSulcusBranch(skeletonBinaryData, leftCSSegmentationCandidate);
		activeCSBranchVisitMatrix = new boolean[skeletonImageProcessor.getWidth()][skeletonImageProcessor.getHeight()];
		
		if(activeCSBranch != null)
		{
			//CSUtility.showBinaryData(imp.getTitle() +  "Active CS Branch", activeCSBranch);
			
			double enlargementRatio = enlargeCSSkeleton(keySliceImageProcessor);
			while(enlargementRatio >= 0.09)
			{
				enlargementRatio = enlargeCSSkeleton(keySliceImageProcessor);
			}
						
			//CSUtility.showBinaryData("CS Branch 2", activeCSBranch);
			return activeCSBranch;
		}
		
		return null;
	}
	
	private boolean[][] segmentLeftCSSulcus(ImageProcessor keySliceImageProcessor)
	{
		// Find bast matching atlas image. originalAtlasImage and mappedAtlasImage variables were set in this function.
		this.findBestLeftCSAtlas(keySliceImageProcessor);

		// Grow region
		this.growRegion(keySliceImageProcessor, true);
		//CSUtility.showBinaryData(imp.getTitle() +  " Initial Segmentation", activeMatrix[currentSliceIndex - 1]);
		
		// Skeletonize image
		ImageProcessor skeletonImageProcessor = new ByteProcessor(keySliceImageProcessor.getWidth(), keySliceImageProcessor.getHeight());		
		skeletonImageProcessor = CSUtility.generateSliceWithBinaryData(activeMatrix[currentSliceIndex - 1]).convertToByteProcessor();
		skeletonImageProcessor.xor(Integer.parseInt("11111111", 2));
		
		Binary binary = new Binary();
		binary.setup("fill", new ImagePlus("Skeleton Image Processor", skeletonImageProcessor));
		binary.run(skeletonImageProcessor);

		skeletonImageProcessor.xor(Integer.parseInt("11111111", 2));

		Skeletonize3D_ skeletonize3d = new Skeletonize3D_();
		skeletonize3d.setup("", new ImagePlus("Skeleton Image Processor", skeletonImageProcessor));
		skeletonize3d.run(skeletonImageProcessor);

		boolean[][] skeletonBinaryData = CSUtility.generateBinaryData(skeletonImageProcessor);

		//CSUtility.showBinaryData(imp.getTitle() +  " SKELETON", skeletonBinaryData);
		
		// Analyze skeletonized version of segmentation and get Central Sulcus branch.
		CSSkeletonAnalyzer skeletonAnalyzer = new CSSkeletonAnalyzer();
		activeCSBranch = skeletonAnalyzer.getCentralSulcusBranch(skeletonBinaryData, leftCSSegmentationCandidate);
		activeCSBranchVisitMatrix = new boolean[skeletonImageProcessor.getWidth()][skeletonImageProcessor.getHeight()];
		
		if(activeCSBranch != null)
		{
			//CSUtility.showBinaryData(imp.getTitle() +  "Active CS Branch", activeCSBranch);
			
			double enlargementRatio = enlargeCSSkeleton(keySliceImageProcessor);
			while(enlargementRatio >= 0.09)
			{
				enlargementRatio = enlargeCSSkeleton(keySliceImageProcessor);
			}
						
			//CSUtility.showBinaryData("CS Branch 2", activeCSBranch);
			return activeCSBranch;
		}
		
		return null;
	}
	
	public double getDiceScore()
	{
		return getDiceScore(activeCSBranch);
	}
	
	/**
	 * This function finds manually segmented pair of the given image
	 * and computes dice score to measure segmentation quality.
	 * 
	 * @param processor Binary data which represents segmentation result
	 * @return dice score value.
	 * */
	private double getDiceScore(boolean[][] binaryData)
	{
		double diceScore = 0;
		try
		{
			String segmenedImagePath = "./../../Data/BAYINDIR_DATA/ManualSegment(ALL)/" + imp.getTitle().substring(0, imp.getTitle().length() - 4) + "_S.hdr";
			
			ImagePlus ij = new ImagePlus(segmenedImagePath);
			
			ij.setSlice(currentSliceIndex);
			ij.show();
			
			int groundTruthDataValue = 0;
			boolean segmentationValue = false;
			
			ImageProcessor segmentationResultProccessor = ij.getProcessor();
			int width = segmentationResultProccessor.getWidth();
			int height = segmentationResultProccessor.getHeight();
			
			int countOfSegmentation = 0;
			int countOfGroundTruthData = 0;
			int countOfIntersectData = 0;
			
			for (int x=0; x < width; x++)
			{
				for(int y=0; y < height; y++)
				{
					groundTruthDataValue = ij.getProcessor().getPixel(x, y);
					segmentationValue = binaryData[x][y];
					
					if(groundTruthDataValue > 0)
					{
						countOfGroundTruthData++;
					}
					
					if(segmentationValue )
					{
						countOfSegmentation++;
					}
					
					if(groundTruthDataValue > 0 && segmentationValue)
					{
						countOfIntersectData++;
					}
				}
			}
			
			diceScore = (2.0d * (double) countOfIntersectData) / ((double) countOfSegmentation + (double) countOfGroundTruthData);
		} 
		catch (Exception e)
		{
			
		}

		return diceScore;
		
	}
	
	
	/**
	 * This function aims to fine tune atlas mapping by translating atlas image
	 * over given processor in a pre-defined window. At each move action
	 * it computes count of matching pixels. 
	 * Then translates image to the position which has maximum matching pixel count.
	 * 
	 * @param processor An ImageProcessor keeps information of the active slice
	 * @param atlasImage BufferedImage which contains given atlas image.
	 * 
	 * @return atlas image which is translated to best matching location..
	 * */
	private BufferedImage fineTuneAtlasMapping(ImageProcessor processor, BufferedImage atlasImage)
	{
		int yWindowUnit = skullRectangle.height / 10;
		int xWindowUnit = skullRectangle.width / 30;
		int maxCount = 0;
		int maxCountX = 0, maxCountY = 0;
				
		BufferedImage returnImage = null;
		
		for(int x=-(xWindowUnit / 2); x < (xWindowUnit / 2); x+=3)
		{
			for(int y=-(yWindowUnit / 2); y < (yWindowUnit / 2); y+=3)
			{
				BufferedImage tempAtlasImage = this.generateMappedAtlasImage(processor, atlasImage, skullRectangle.x + x, skullRectangle.y + y);
				int candidatePointCount = this.markCandidatePointsWithAtlasImage(processor, tempAtlasImage, true, false);
				if(candidatePointCount > maxCount)
				{
					maxCount = candidatePointCount;
					maxCountX = x;
					maxCountY = y;
					returnImage = tempAtlasImage;
				}
			}
		}
		
//		// Translate image to best location
//		AffineTransform transform = new AffineTransform();
//	    transform.translate((double)maxCountX, (double)maxCountY);
//	    AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
//	    atlasImage = op.filter(atlasImage, null);
//		
	    return returnImage;
		

	}
	
	/**
	 * This function aims to find best atlas image for left side of CS. Project contains six different
	 * atlas images which are belong to different genders and patients having different
	 * atrophy levels
	 * 
	 *  
	 * Algorithm tries all the atlases and computes best match by comparing atlas
	 * pixels with the given processor voxels. 
	 * 
	 * @param processor An ImageProcessor keeps information of the active slice

	 * */
	private void findBestLeftCSAtlas(ImageProcessor processor)
	{
		String atlasImageName = "";
		int maxCount = 0;
		
		if(skullRectangle == null)
		{
			skullRectangle = this.getSkullRectangle(processor);
		}
		
		//for (int i=1; i <=6; i++)
		{
			//atlasImageName = "AtlasLeft"+i+".png";
			atlasImageName = "AtlasLeft.png";
			BufferedImage tempOriginalAtlasImage = this.loadAtlasImage(processor, atlasImageName);
			tempOriginalAtlasImage = this.correctImageRotation(processor, tempOriginalAtlasImage);
			
			BufferedImage tempMappedAtlasImage = this.generateMappedAtlasImage(processor, tempOriginalAtlasImage, skullRectangle.x, skullRectangle.y);
			tempMappedAtlasImage = this.fineTuneAtlasMapping(processor, tempOriginalAtlasImage);
			
			int candidatePointCount = this.markCandidatePointsWithAtlasImage(processor, tempMappedAtlasImage, true, false);
			if(candidatePointCount > maxCount)
			{
				maxCount = candidatePointCount;
			//	maxCountIndex = i;
				mappedAtlasImage = tempMappedAtlasImage;
				originalAtlasImage = tempOriginalAtlasImage;
			}
		}
		
		//System.out.println("Used atlas image ("+ atlasImageName +")");
		
		//this.drawImageAsOverlay(processor, mappedAtlasImage, 0, 0, 120);

		// Mark candidate points with atlas
		this.markCandidatePointsWithAtlasImage(processor, mappedAtlasImage, true, true);

	}
	
	/**
	 * This function aims to find best atlas image for right side of CS. Project contains six different
	 * atlas images which are belong to different genders and patients having different
	 * atrophy levels
	 * 
	 *  
	 * Algorithm tries all the atlases and computes best match by comparing atlas
	 * pixels with the given processor voxels. 
	 * 
	 * @param processor An ImageProcessor keeps information of the active slice

	 * */
	private void findBestRightCSAtlas(ImageProcessor processor)
	{
		String atlasImageName = "";
		int maxCount = 0;
		
		if(skullRectangle == null)
		{
			skullRectangle = this.getSkullRectangle(processor);
		}
		

			atlasImageName = "AtlasRight.png";
			BufferedImage tempOriginalAtlasImage = this.loadAtlasImage(processor, atlasImageName);
			tempOriginalAtlasImage = this.correctImageRotation(processor, tempOriginalAtlasImage);
			
			BufferedImage tempMappedAtlasImage = this.generateMappedAtlasImage(processor, tempOriginalAtlasImage, skullRectangle.x, skullRectangle.y);
			tempMappedAtlasImage = this.fineTuneAtlasMapping(processor, tempOriginalAtlasImage);
			
			int candidatePointCount = this.markCandidatePointsWithAtlasImage(processor, tempMappedAtlasImage, false, false);
			if(candidatePointCount > maxCount)
			{
				maxCount = candidatePointCount;
			//	maxCountIndex = i;
				mappedAtlasImage = tempMappedAtlasImage;
				originalAtlasImage = tempOriginalAtlasImage;
			}
		
		
		//System.out.println("Used atlas image ("+ atlasImageName +")");
		
		this.drawImageAsOverlay(processor, mappedAtlasImage, 0, 0, 120);

		// Mark candidate points with atlas
		this.markCandidatePointsWithAtlasImage(processor, mappedAtlasImage, false, true);

	}
	
	
	/**
	 * This function maps given atlas image over given processor coordinate space and returns
	 * mapped atlas image instance.
	 * 
	 * @param processor An ImageProcessor keeps information of the active slice
	 * @param processor An Atlas Image
	 * @return mapped atlas image
	 * */
	private BufferedImage generateMappedAtlasImage(ImageProcessor processor, BufferedImage atlasImage, int xLocation, int yLocation)
	{
		MidsaggitalUtil util = new MidsaggitalUtil();
		util.imp = this.imp;

		BufferedImage mappedAtlasImage = new BufferedImage(processor.getWidth(), processor.getHeight(), atlasImage.getType());
		Graphics2D gc = mappedAtlasImage.createGraphics();
		
		gc.drawImage(atlasImage, xLocation, yLocation, null);
		gc.dispose();

		return mappedAtlasImage;
	}
	
	/**
	 * This function corrects atlas image rotation. It finds midsagittal plane using
	 * Ransac technique
	 * (http://www.eurasip.org/Proceedings/Eusipco/Eusipco2006/papers
	 * /1568982240.pdf) on given slice and computes slope of the axis. Rotates
	 * each slice using the angle of slope MidsagittalUtil class is responsible
	 * for operations and MidsagittalInformation class stores information such
	 * as slope, axis points etc.
	 * 
	 * @param processor An ImageProcessor keeps information of the active slice
	 * @param processor An Atlas Image
	 * @return rotated atlas image
	 * */
	private BufferedImage correctImageRotation(ImageProcessor processor, BufferedImage atlasImage)
	{
		MidsaggitalUtil util = new MidsaggitalUtil();
		util.imp = this.imp;

		MidsaggitalInformation info = util.getMidsaggitalInformation(processor);

		AffineTransform transform = new AffineTransform();
	    transform.rotate((-info.rotationAngle * Math.PI) / 180 , atlasImage.getWidth()/2, atlasImage.getHeight()/2);
	    AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
	    atlasImage = op.filter(atlasImage, null);
		
	    return atlasImage;
	}

	/**
	 * This function finds the keys slice which represents CS with its
	 * characteristic omega shape. Here, algorithm first searches for the
	 * maximum slice (having the maximum counts of visible pixels). Keys slice
	 * is located above the maximum slice and its volume ratio is 0.80 compare
	 * to maximum slice.
	 * */
	private void findKeySlice(boolean useCurrentSlice)
	{
		
		
		
		// Init pixelCount and normalizedValues arrays
		stackSlicePixelCount = new int[stack.getSize()];
		normalizedValues = new double[stack.getSize()];


		// Get auto threshold value for each slice in the stack
		// and increase pixel count value for each pixel which has higher gray
		// value then threshold.
		for (int i = 1; i <= stack.getSize(); i++) // Visit every slice in the
													// stack
		{
			ImageProcessor processor = stack.getProcessor(i).convertToFloat(); // Get
																				// RGB
																				// processor
			int count = 0;

			double threshold = processor.getStatistics().mean; // Get threshold
			for (int x = 0; x < processor.getWidth(); x++)
			{
				for (int y = 0; y < processor.getHeight(); y++)
				{
					float value = processor.getPixelValue(x, y);

					if (value > threshold) // Increase pixel count if the gray
											// value is higher than threshold
					{
						count++;
					}
				}
			}

			stackSlicePixelCount[i - 1] = count; // Add value to
													// pixelCountsArray

			// Set maximum pixel count
			if (count > maximumPixelCount)
			{
				maximumPixelCount = count;
				maximumSliceIndex = i;
			}
		}

		// Compute normalized values
		for (int i = 0; i < stack.getSize(); i++)
		{
			normalizedValues[i] = (float) stackSlicePixelCount[i] / (float) maximumPixelCount;
			 //tw.append((i + 1) + "\t" + stackSlicePixelCount[i] + "\t" + normalizedValues[i] + "\t");
		}

		/*
		 * Slice order would be different at each image stack. To find slice
		 * order, we will compare first and last slice's pixel counts and assume
		 * that slice with small pixel count number represents top of the skull.
		 */

		slicesDirection = (stackSlicePixelCount[0] > stackSlicePixelCount[stackSlicePixelCount.length - 1]) ? 1 : -1;

		// Find key slice
		if(!useCurrentSlice)
		{
			double currentMinimum = 100.0f;
			for (int i = maximumSliceIndex; (i > 0 && i < stack.getSize() - 1); i += slicesDirection)
			{
				// Get minimum normalized value which is higher than 0.80
				//System.out.println("index("+i+"), value("+normalizedValues[i-1]+"), currentMinimum("+currentMinimum+")");
				if (normalizedValues[i-1] > 0.800000d && normalizedValues[i-1] < currentMinimum)
				{
					currentMinimum = normalizedValues[i-1];
					currentSliceIndex = i;
				}
			}
		}
		else
		{
			currentSliceIndex = imp.getCurrentSlice();
		}

		imp.setPosition(currentSliceIndex); // Move stack to the key slice
	}

	/**
	 * This function gets plot data over midsagittal axis and finds first and
	 * last maximums which represent upper and lower limits of the skull bounds
	 * respectively. It finds the geometric center of the skull along
	 * midsagittal axis and creates a virtual axis which is perpendicular to
	 * midsagittal axis and crossing its center. Finds left and right bounds of
	 * the skull similarly by searching first and last grey value maximums over
	 * this new axis and computes skull rectangle.
	 * 
	 * @param processor
	 *            An ImageProcessor keeps information of the active slice
	 * @return the rectangle of the skull.
	 * */
	private Rectangle getSkullRectangle(ImageProcessor keySliceImageProcessor)
	{
		int x, y;
		double mean = (keySliceImageProcessor.getStatistics().mean) * 1.5;

		// Visit pixels on midsagittal plane and try to find maximum values to
		// highlight upper and lower limits of the skull
		MidsaggitalInformation info = new MidsaggitalUtil().getMidsaggitalInformation(keySliceImageProcessor);

		int minimumLeftBoundryX = Integer.MAX_VALUE;
		int minimumRighBoundryX = 0;
		int minimumTopBoundryY = Integer.MAX_VALUE;
		int minimumBottomBoundryY = 0;
		
		
		for (y = 0; y < keySliceImageProcessor.getHeight(); y++)
		{			
			int previousBoundryIntensity = 0;
			
			// Find left bound
			for (x = 0; x < keySliceImageProcessor.getWidth(); x++)
			{
				int pixelValue = keySliceImageProcessor.getPixel(x, y);
				if(pixelValue > mean) // That's a candidate point
				{
					if(pixelValue < previousBoundryIntensity) // value is not going up. Mark this point as local maximum
					{
						if(x < minimumLeftBoundryX)
						{
							minimumLeftBoundryX = x;
						}
						break;
					}
					previousBoundryIntensity = pixelValue;
				}
			}	
			
			previousBoundryIntensity = 0;
			// Find right bound
			for (x = keySliceImageProcessor.getWidth() - 1; x >=0; x--)
			{
				int pixelValue = keySliceImageProcessor.getPixel(x, y);
				if(pixelValue > mean) // That's a candidate point
				{
					if(pixelValue < previousBoundryIntensity) // value is not going up. Mark this point as local maximum
					{
						if(x > minimumRighBoundryX)
						{
							minimumRighBoundryX = x;
						}
						break;
					}
					previousBoundryIntensity = pixelValue;
				}
			}	
		}

		for (x = 0; x < keySliceImageProcessor.getWidth(); x++)
		{			
			int previousBoundryIntensity = 0;
			
			// Find top bound
			for (y = 0; y < keySliceImageProcessor.getHeight(); y++)
			{
				int pixelValue = keySliceImageProcessor.getPixel(x, y);
				if(pixelValue > mean) // That's a candidate point
				{
					if(pixelValue < previousBoundryIntensity) // value is not going up. Mark this point as local maximum
					{
						if(y < minimumTopBoundryY)
						{
							minimumTopBoundryY = y;
						}
						break;
					}
					previousBoundryIntensity = pixelValue;
				}
			}	
			
			previousBoundryIntensity = 0;
			// Find bottom bound
			for (y = keySliceImageProcessor.getHeight() - 1; y >=0; y--)
			{
				int pixelValue = keySliceImageProcessor.getPixel(x, y);
				if(pixelValue > mean) // That's a candidate point
				{
					if(pixelValue < previousBoundryIntensity) // value is not going up. Mark this point as local maximum
					{
						if(y > minimumBottomBoundryY)
						{
							minimumBottomBoundryY = y;
						}
						break;
					}
					previousBoundryIntensity = pixelValue;
				}
			}	
		}

		Rectangle skullRectangle = new Rectangle(minimumLeftBoundryX, minimumTopBoundryY, Math.abs(minimumRighBoundryX - minimumLeftBoundryX), Math.abs(minimumBottomBoundryY - minimumTopBoundryY));
		return skullRectangle;
	}

	/**
	 * This function registers default atlas image over given
	 * slice(ImageProcessor) For registration operation; it calculates skull the
	 * region and scales atlas image into this coordinate system.
	 * 
	 * @param processor
	 *            An ImageProcessor keeps information of the active slice
	 * @return the registered atlas image.
	 * */
	private BufferedImage loadAtlasImage(ImageProcessor processor, String atlasImageName)
	{
		try
		{
			Point atlasCenterOfMass = new Point(163, 185);
			double xScaleCoef, yScaleCoef;
			
			
			BufferedImage atlasImage = ImageIO.read(new File("./../" + atlasImageName));


			BufferedImage resized = new BufferedImage(skullRectangle.width, skullRectangle.height, atlasImage.getType());
			Graphics2D g = resized.createGraphics();
			g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			g.drawImage(atlasImage, 0, 0, skullRectangle.width, skullRectangle.height, 0, 0, atlasImage.getWidth(), atlasImage.getHeight(), null);
			g.dispose();

			xScaleCoef = (double) resized.getWidth() / (double)atlasImage.getWidth();
			yScaleCoef = (double) resized.getHeight() / (double)atlasImage.getHeight();
			
			atlasCenterOfMass = new Point((int)(atlasCenterOfMass.x * xScaleCoef), (int)(atlasCenterOfMass.y * yScaleCoef));
			
			// Get skull cropped image
			ImageProcessor cropped = processor.duplicate();
			cropped.setRoi(skullRectangle);
			cropped = cropped.crop();
			Point imageCenterOfMass = new Point((int)cropped.getStatistics().xCenterOfMass, (int)cropped.getStatistics().yCenterOfMass);
			
			BufferedImage remappedAtlas = new BufferedImage(resized.getWidth(), resized.getHeight(), atlasImage.getType());
			Graphics2D newg = remappedAtlas.createGraphics();
			//newg.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			
						
			newg.drawImage(resized, 0, 0, imageCenterOfMass.x, imageCenterOfMass.y, 
								    0, 0, atlasCenterOfMass.x, atlasCenterOfMass.y, null);
			newg.drawImage(resized, 0, imageCenterOfMass.y, imageCenterOfMass.x, resized.getHeight(), 
								 0, atlasCenterOfMass.y, atlasCenterOfMass.x, resized.getHeight(), null);
//			g.drawImage(resized, 0, imageCenterOfMass.x, resized.getWidth(), imageCenterOfMass.y, 
//					 			 0, atlasCenterOfMass.x, atlasCenterOfMass.x, atlasCenterOfMass.y, null);
			newg.dispose();
			
			
			atlasImage = resized;

			return atlasImage;

		} catch (Exception e)
		{
			System.out.println("Atlas image couldn't be found at path.");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * This function compares gray intensity of the given processor image with
	 * the given atlas. Marks the pixel as candidate if its value is lower than
	 * image threshold value, which is calculated based on image histogram.
	 * 
	 * @param processor An ImageProcessor keeps information of the active slice
	 * @param processor An image stores atlas image
	 * @return Count of candidate points
	 * */
	private int markCandidatePointsWithAtlasImage(ImageProcessor processor, BufferedImage atlasImage, boolean isLeftAtlasImage, boolean findMaximimPoints)
	{
		int x, y;
		int totalCandidatePointCount = 0;
		
		Point maximumPoint = new Point((isLeftAtlasImage) ? 0 : processor.getWidth() - 1, 0);
		
		// Clean active matrix
		activeMatrix[currentSliceIndex - 1] = new boolean[processor.getWidth()][processor.getHeight()]; // [translatedX][translatedX] = true;
		newPointsAdded = false;
		
		double mean = (processor.getStatistics().mean) * 1.6;

		boolean drawCandidatePoints = false;
		
		if (drawCandidatePoints)
		{
			processor.setColor(Color.WHITE);
		}
		
		// Add initial points
		for (x = 0; x < atlasImage.getWidth(); x++)
		{
			for (y = 0; y < atlasImage.getHeight(); y++)
			{
				int atlasRGBValue = atlasImage.getRGB(x, y);
				int keySliceRGBValue = processor.getPixel(x, y);
				

				if ((atlasRGBValue != -1 && atlasRGBValue !=0 && atlasRGBValue !=-16777216) && keySliceRGBValue < mean && ((isLeftAtlasImage) ? x > maximumPoint.x : x < maximumPoint.x ))
				{
					totalCandidatePointCount ++;
					newPointsAdded = true;

					if(findMaximimPoints)
					{
					//	activeMatrix[currentSliceIndex - 1][x][y] = true;
						maximumPoint = new Point(x, y);
					}
					else
					{
						activeMatrix[currentSliceIndex - 1][x][y] = true;
					}
					
					if (drawCandidatePoints)
					{
						processor.drawPixel(x, y);
						System.out.println("Setting value : x" + x + ", y:" + y +", value:" + atlasRGBValue);

					}
				}
			}
		}
		
		if(findMaximimPoints && maximumPoint.x > 0)
		{
			activeMatrix[currentSliceIndex - 1][maximumPoint.x][maximumPoint.y] = true;
			leftCSSegmentationCandidate = maximumPoint;
		}
		
		return totalCandidatePointCount;
	}

	
	private ArrayList<CSCandidatePointCluster> findCandidatePointClusters(boolean[][] data)
	{
		int width = data.length;
		int height = data[0].length;
		
		boolean[][] visitedData = new boolean[width][height];
		
		
		for(int x=0; x < width; x++)
		{
			for(int y=0; y < height; y++)
			{
				
			}
		}
		
		return null;
	}
	
//	private ArrayList<Point> findCandidatePointClusters(boolean[][] data)
//	{
//		int width = data.length;
//		int height = data[0].length;
//		
//		boolean[][] visitedData = new boolean[width][height];
//		
//		
//		for(int x=0; x < width; x++)
//		{
//			for(int y=0; y < height; y++)
//			{
//				
//			}
//		}
//		
//		return null;
//	}
	
	
	/**
	 * This function takes a two dimensional array (central sulcus segmented skeleton branch)
	 * and enlarges selection. At each iteration, it gathers ratio of enlargement on central sulcus
	 * segmentation area. Stops if the enlargement ratio is lower than predefined threshold.
	 * 
	 * @param processor An ImageProcessor keeps information of the active slice
	 * @return segmentation enlargement ratio.
	 * */
	private double enlargeCSSkeleton(ImageProcessor processor)
	{
		double mean = (processor.getStatistics().mean) * 1.6;
		int addedPixelCount = 0;
		int originalCount = 0;
		
		boolean activeMatrix[][] = new boolean[processor.getWidth()][processor.getHeight()];
		
		for (int x = 1; x < processor.getWidth() - 1; x++)
		{
			for (int y = 1; y < processor.getHeight() - 1; y++)
			{
				boolean value = activeCSBranch[x][y];
				
				if(value)
				{
					
					originalCount++;
					activeCSBranchVisitMatrix[x][y] = true; // set visited
					activeMatrix[x][y] = true; // set visited
					
					// get unvisited neighbors
					for (int nx = -1; nx < 2; nx++)
					{
						for (int ny = -1; ny < 2; ny++)
						{
							if(!activeCSBranchVisitMatrix[x + nx][y + ny])
							{
								if(processor.getPixel(x + nx, y + ny) < mean)
								{
									activeMatrix[x + nx][y + ny] = true;
									addedPixelCount++;
									
									
								}
								activeCSBranchVisitMatrix[x + nx][y + ny] = true;
							}
						}
					}					
				}
			}
		}
		
		double ratio = (double)addedPixelCount / (double)csBranchSegmentedPixelCount;
		csBranchSegmentedPixelCount += addedPixelCount;
		activeCSBranch = activeMatrix;
		
		//System.out.println("Enlargement ratio ("+ ratio +")");
		return ratio;

	}
	
	/**
	 * This function starts with candidate points and grows region by visiting
	 * their each neighbor pixels. It continuously tries to enlarge area by
	 * adding new pixels into. Stops when there is no new pixel added into
	 * region.
	 * 
	 * @param processor An ImageProcessor keeps information of the active slice
	 * @param isLeftHemisphere A boolean value which defines left or right hemisphere of the skull 
	 * */
	private void growRegion(ImageProcessor keySliceImageProcessor, boolean isLeftHemisphere)
	{
		double mean = (keySliceImageProcessor.getStatistics().mean) * 1.6;
		
		int neighborX = 0, neighborY = 0;
		int atlasImageX = 0;
		int atlasImageY = 0;
		try
		{
			Rectangle skullRectangle = this.getSkullRectangle(keySliceImageProcessor);

			Rectangle hemisphereArea = null;
			
			if(isLeftHemisphere)
			{
				hemisphereArea = new Rectangle(0, keySliceImageProcessor.getHeight() / 3, keySliceImageProcessor.getWidth() / 2, (keySliceImageProcessor.getHeight() / 3) * 2);
			}
			else
			{
				hemisphereArea = new Rectangle(keySliceImageProcessor.getWidth() / 2, keySliceImageProcessor.getHeight() / 3, keySliceImageProcessor.getWidth(), (keySliceImageProcessor.getHeight() / 3) * 2);
			}
			
			while (newPointsAdded)
			{
				newPointsAdded = false;

				for (int activeMatrixX = 1; activeMatrixX < keySliceImageProcessor.getWidth() - 1; activeMatrixX++)
				{
					for (int activeMatrixY = 1; activeMatrixY < keySliceImageProcessor.getHeight() - 1; activeMatrixY++)
					{
						if (activeMatrix[currentSliceIndex - 1][activeMatrixX][activeMatrixY] == true) // This
						// point
						// is
						// candidate.
						{
							// Visit neighbor pixels and enlarge selection
							for (int x = -1; x < 2; x++)
							{
								for (int y = -1; y < 2; y++)
								{
									neighborX = activeMatrixX + x;
									neighborY = activeMatrixY + y;
									atlasImageX = neighborX - skullRectangle.x;
									atlasImageY = neighborY - skullRectangle.y;

									int rgbValue = keySliceImageProcessor.getPixel(neighborX, neighborY);

									if (activeMatrix[currentSliceIndex - 1][neighborX][neighborY] == false 
											&& rgbValue < mean
											&& hemisphereArea.contains(neighborX, neighborY)
									)
									{
										
										activeMatrix[currentSliceIndex - 1][neighborX][neighborY] = true;
										newPointsAdded = true;
									}
								}
							}
						}
					}
				}
			}

			//this.drawRegion(keySliceImageProcessor);

		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			System.out.println("Active X:" + neighborX + ", active Y:" + neighborY);
			System.out.println("AtlasImage X:" + atlasImageX + ", AtlasImage Y:" + atlasImageY);
			e.printStackTrace();
		}
	}

	/**
	 * This function draws segmentation region as an overlay image.
	 * 
	 * @param processor
	 *            An ImageProcessor keeps information of the active slice
	 * */
	private void drawRegion(ImageProcessor keySliceImageProcessor)
	{
		Overlay overlay;

		if (imp.getOverlay() != null)
		{
			overlay = imp.getOverlay();
			imp.getOverlay().clear();
		} else
		{
			overlay = new Overlay();
			imp.setOverlay(overlay);
		}

		ImageProcessor p = new ColorProcessor(keySliceImageProcessor.getWidth(), keySliceImageProcessor.getHeight());
		p.setColor(Color.RED);

		for (int activeMatrixX = 0; activeMatrixX < keySliceImageProcessor.getWidth(); activeMatrixX++)
		{
			for (int activeMatrixY = 0; activeMatrixY < keySliceImageProcessor.getHeight(); activeMatrixY++)
			{
				if (activeMatrix[currentSliceIndex - 1][activeMatrixX][activeMatrixY] == true)
				{
					p.drawPixel(activeMatrixX, activeMatrixY);
				}
			}
		}


		ImageRoi imageRoi = new ImageRoi(0, 0, p);
		imageRoi.setZeroTransparent(true);
		overlay.add(imageRoi);
		keySliceImageProcessor.drawOverlay(overlay);

	}

	
	/**
	 * This function draws segmentation region as an overlay image.
	 * 
	 * @param processor
	 *            An ImageProcessor keeps information of the active slice
	 * */
	private void drawImageAsOverlay(ImageProcessor keySliceImageProcessor, BufferedImage image, int x, int y, int transparency)
	{
		Overlay overlay;

		if (imp.getOverlay() != null)
		{
			overlay = imp.getOverlay();
			imp.getOverlay().clear();
		} else
		{
			overlay = new Overlay();
			imp.setOverlay(overlay);
		}

		ImageRoi imageRoi ;
		
		// Make image semi-transparent
		if(transparency < 255)
		{
			BufferedImage redderImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
			
			for (int ix = 0; ix < image.getWidth(); ++ix) 
			{
				for (int iy = 0; iy < image.getHeight(); ++iy) {
				
				int rgb = image.getRGB(ix,iy);
				
				int new_alpha = 100;                        // Maximum!
				int new_red =   (rgb & 0x00FF0000) >>> 16;  // Copy original redness
				int new_green = (rgb & 0x0000FF00) >>> 8;   // Copy original greenness
				int new_blue  = (rgb & 0x000000FF) >>> 0;   // Copy original blueness
				
				int new_rgb = (new_alpha << 24) | (new_red << 16) | (new_green << 8) | new_blue;
				redderImage.setRGB(ix, iy, new_rgb);
				}
			}
			
			imageRoi = new ImageRoi(x, y, redderImage);
		}
		else
		{
			imageRoi = new ImageRoi(x, y, image);
		}
		
		overlay.add(imageRoi);
		keySliceImageProcessor.drawOverlay(overlay);

	}
	
	
//	private ImageProcessor skeletonizeImage(ImageProcessor processor)
//	{
//		ImageStack _stack = new ImageStack(processor.getWidth(), processor.getHeight());
//
//		_stack.addSlice(processor);
//		
//		ImagePlus plus = new ImagePlus("Segmentation Results", _stack);
//		
//		Skeletonize3D_ skeletonizor = new Skeletonize3D_();
//		plus.setSlice(keySliceIndex);
//		skeletonizor.setup("", plus);
//		skeletonizor.run(plus.getProcessor());
//		
//		plus.show();
//		
//		return plus.getProcessor();
//	}
	
//	
//	/**
//	 * Generates new stack based on key slice..
//	 * 
//	 * @param processor
//	 *            An ImageProcessor keeps information of the active slice
//	 * */
//	private void showSegmentationResults(String name, ImageProcessor keySliceImageProcessor)
//	{
//		ImageStack _stack = new ImageStack(keySliceImageProcessor.getWidth(), keySliceImageProcessor.getHeight());
//
//		for (int i = 0; i < stack.getSize(); i++)
//		{
//			ImageProcessor processor = new  ByteProcessor(keySliceImageProcessor.getWidth(), keySliceImageProcessor.getHeight());
//
//			drawSegmentationMatrixIntoImage(i, processor);
//
//			_stack.addSlice(processor);
//		}
//
//		ImagePlus plus = new ImagePlus(name, _stack);
//		plus.setSlice(keySliceImageProcessor.getSliceNumber());
//		plus.show();
//		
//		// Initialize AnalyzeSkeleton_
//				
//
//				
//	}

}
