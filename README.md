# Central Sulcus Segmentation Plugin for ImageJ #

This plug-in works with ImageJ and aims to segment Central Sulcus area over MR images. 
Algorithm first tries to detect the "Key Slice" in the image stack, where the central sulcus is visible with its characteristic omega shape. After detection of the "Key Slice" it registers the atlas image(Atlas.png) over the slice and marks candidate points. Then enlarges selection through neighbor pixels using region growing technique. Finally, the algorithm tries to reduce false positive pixels by using region mapping image (AtlasRegion.png)

## Installation  ##

### Requirements ###
* Eclipse Java Editor Installation (www.eclipse.org)
* JDK 1.6 (for ImageJ compatibility)
* Git Installation (http://git-scm.com/downloads)

### Installing and Compiling Sources ###
* Clone source codes into your working directory using git : (git clone https://oguzdemir@bitbucket.org/oguzdemir/csplugin.git)
You must see (CS, IJ) folders and (Atlas.png, AtlasRegion.png, CSPlugin_ build.xml.launch, IJ.launch) files.
* Launch Eclipse and go to "File->Import->General->Existing Projects into Workspace" menu and import both IJ, CS projects from your working directory where you cloned sources from git repository.
* Go to  "File->Import->Run/Debug->Launch Configurations" menu on Eclipse and import CSPlugin_ build.xml.launch, IJ.launch files. This action should create a run configuration under run menu with "IJ" name.
* When you build the project, it should compile sources using build.xml file and copy the compiled plugin.jar file into IJ/plugins folder.
* Run the app using "IJ" run configuration. It should launch ImageJ and segmentation plugin should be under "Plugins->CS->Segment CS" menu.

